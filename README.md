# Assignment 2 - Agile Software Practice.

Name: Yuting Jing

## Client UI.
Web API GitLab repo:https://gitlab.com/Leslie_Lora/cicdapi.git 
Client GitLab repo: https://gitlab.com/Leslie_Lora/vue-cicd-2.git
![][Home]
>>Home has daily recommendation of part of a poem. You can click the link to read the complete poem.

![][Look Poem]
>>After user click the link in the home page, the web will lead user to this page to read the whole version of the poem

![][Authors]
>>All authors shows all authors in the page.

![][AllPoems]
>>All poems shows all the poems in the page.

![][Register]
>>User register in this page. It checks whether the information user enters are valid.

![][Login]
>>User login here. If the User name or passwords don't match, it will have an alter window to tell user to enter the info again.

![][MyPoem]
>>Before user login, the table in this page will be empty. If user doesn't have any poem of his or her own, the table will be empty. 

>>User can add a poem by clicking the button and the web will lead user to another page to enter the info about his or her peom



## E2E/Cypress testing.

## Web API CI.

## GitLab CI.




[Home]: ./img/Home.png
[Authors]:./img/Authors.png
[AllPoems]:./img/AllPoems.png
[Register]:./img/Register.png
[Login]:./img/Login.png
[MyPoem]:./img/MyPoem.png
[Look Poem]:./img/Look Poem.png